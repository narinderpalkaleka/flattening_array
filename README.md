# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This program is written in Python and converts an array of arbitrarily nested arrays into a simplified or flattened array.

### How do I get set up? ###

* You will need to have Python installed
* Run the program in terminal
* Add multi-layered arrays in the source file to get a flattened result

### Contribution guidelines ###

* For contribution, create an issue on bitbucket repo

### Who do I talk to? ###

* Narinderpal Kaleka, nkaleka4160@conestogac.on.ca

### License

* This Source Code Software may contain source code that, unless expressly licensed for other purposes, is provided solely for reference purposes pursuant to the terms of this Agreement. Source code may not be redistributed unless expressly provided for in this Agreement. This code is property of Narinderpal Singh Kaleka.