#A Python program to convert arbitrarily nested arrays to a flat array
#It takes an input and if its an array it flattens it

#flatten function to flatten the array
def flatten(input_array):
    #an empty result array to store the flattened result
    result_array = []

    #if the input is not an array return with a message
    if not isinstance(input_array, list):
        return "The input is not an array"

    #iterate every element in input array
    for element in input_array:
        #if the element is an integer, add it to result array
        if isinstance(element, int):
            result_array.append(element)
            #if the list contains nested list, recursively call the function
        elif isinstance(element, list):
            result_array += flatten(element)
            #If the list is not a nested array or integers
        else:
            return "The List is not of arrays or integers!"
    #Returning the flattened array
    return (result_array)

#the main function
def main():
    #testing for a nested array
    in_array = [[1,2,[3]],4]
    print (flatten(in_array))

    #testing for empty array
    in_array = []
    print (flatten(in_array))
    
    #testing for non integer array
    in_array = ['2']
    print (flatten(in_array))
    
    #testing for a scalar
    in_array = 6
    print (flatten(in_array))

#Calling the main function
if __name__ == '__main__':
    main()